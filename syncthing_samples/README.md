1) create two sync folders for each docker (e.x. dirs `Sync_1` and `Sync_2` in `/user/` directory)
2) write paths to this dirs into docker-compose containers volumes
3) run `docker-compose -f double_sync_local.docker-compose.yml up` in this directory to up 2 containers with SyncThing, 
mapped to 2 different folders on local machine. 
4) copy any file to directory `./Sync_1/Sync` and see this file in `./Sync_2/Sync`
Done!!!