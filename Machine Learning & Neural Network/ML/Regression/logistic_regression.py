import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, roc_auc_score
import matplotlib.pyplot as plt

########################## digits task ########################################################

from sklearn.datasets import load_digits
digits = load_digits()

print("Image Data Shape" , digits.data.shape) # dimension of dataset
print("Label Data Shape", digits.target.shape)

plt.figure(figsize=(20,4)) # output
for index, (image, label) in enumerate(zip(digits.data[0:5], digits.target[0:5])):
    plt.subplot(1, 5, index + 1)
    plt.imshow(np.reshape(image, (8,8)), cmap=plt.cm.gray)
    plt.title('Training: %i\n' % label, fontsize = 20)
plt.show()

train_features, test_features, train_target, test_target = train_test_split(digits.data, digits.target, test_size=0.2)

digits_classificator = LogisticRegression()

digits_classificator.fit(train_features, train_target)

predictions = digits_classificator.predict(test_features)

print(accuracy_score(test_target, predictions))

######################################## Task with mobile phone clients ########################################

data = pd.read_csv('E:\Project\practice-loopa\ML & NN\ML\LogisticTrees\\train.csv')

num_cols = [
    'ClientPeriod',
    'MonthlySpending',
    'TotalSpent'
] # Числовые признаки

cat_cols = [
    'Sex',
    'IsSeniorCitizen',
    'HasPartner',
    'HasChild',
    'HasPhoneService',
    'HasMultiplePhoneNumbers',
    'HasInternetService',
    'HasOnlineSecurityService',
    'HasOnlineBackup',
    'HasDeviceProtection',
    'HasTechSupportAccess',
    'HasOnlineTV',
    'HasMovieSubscription',
    'HasContractPhone',
    'IsBillingPaperless',
    'PaymentMethod'
] # Категориальные признаки

feature_cols = num_cols + cat_cols
target_col = 'Churn'

data.isna().sum() # подсчет НаНов по колонкам, вывод в виде Series
print(data.info())

# Заметим, что в колонке TotalSpent данные - строки. Если посмотреть внимательно, то есть пустые строки, что может в дальнейшем может привести к ошибке
# Тут идет конвертация данных в числовые
data['TotalSpent'] = data['TotalSpent'].str.replace(' ','0')
data['TotalSpent'][1048]
data['TotalSpent'] = pd.to_numeric(data['TotalSpent'])

print("Гистограммы числовых признаков")
for i in num_cols:
  plt.hist(data[i].values, label=i)
  plt.legend()
  plt.show()
print("Гистограммы категориальных признаков по отдельности")
for j in cat_cols:
  print(j)
  plt.bar(data[j].value_counts().index, data[j].value_counts().values, label=j, width=0.5, align='edge')
  plt.legend()
  plt.show()
print("Гистограмма всех категориальных признаков")
fig, ax = plt.subplots()
for j in cat_cols:
  #print(j)
  ax.bar(data[j].value_counts().index, data[j].value_counts().values, label=j, width=0.5, align='edge')
  ax.legend()
plt.show()
plt.bar(data[target_col].value_counts().index, data[target_col].value_counts().values, label=j, width=0.5)

data.drop("HasPhoneService", axis=1)
data.drop("IsSeniorCitizen", axis=1)

from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, RobustScaler, LabelEncoder, OneHotEncoder
from sklearn.pipeline import make_pipeline

# learn by regression

# преобразую данные. В фрейме X категориальные данные преобразованы для обучения LogisticRegression,
# в X_origin данные не изменены (потом будет использован в catboost) 
numeric_data = data[num_cols]
categorial_data = data[cat_cols]
scaler = StandardScaler()
X_scaled = pd.DataFrame(scaler.fit_transform(numeric_data))
dummy_features = pd.get_dummies(categorial_data)
X = pd.concat([X_scaled, dummy_features], axis=1)
X_origin = data.iloc[:, :-1]
y = data['Churn']


# делаю 2 метод с кросс-валидацией
parameters = {'C': [100, 10, 1, 0.1, 0.01, 0.001]}

'''pipe = make_pipeline(
    #LabelEncoder(),
    OneHotEncoder(handle_unknown = 'ignore', sparse=False),
    LogisticRegression(solver='lbfgs', max_iter=1000)
)'''

CV_model = GridSearchCV(LogisticRegression(max_iter=10000), 
                        param_grid=parameters,
                        cv=5, 
                        scoring='roc_auc',
                        n_jobs=-1)

CV_model.fit(X.to_numpy(), y)

means = CV_model.cv_results_['mean_test_score']
print(means)
# best C is 10

train_dataset, test_dataset, train_target, test_target = train_test_split(X, y, test_size = 0.2)

best_classificator = LogisticRegression(max_iter=10000, C=10, n_jobs=3)

best_classificator.fit(train_dataset, train_target)
predictions = best_classificator.predict(test_dataset)

print(accuracy_score(test_target, predictions))

####################### MNIST task ###########################################

# mnist_data = pd.read_csv('E:\Project\practice-loopa\ML & NN\ML\Regression\mnist.csv')

# feature_matrix = mnist_data.iloc[:, 1:]
# target = mnist_data.iloc[:, :1]

# train_feature_matrix, test_feature_matrix, train_target, test_target = train_test_split(feature_matrix, target, test_size=0.2)

# print(train_feature_matrix.shape)

# classificator = LogisticRegression(n_jobs=2, max_iter=1000)
# classificator.fit(train_feature_matrix, train_target)

# y_pred = classificator.predict(test_feature_matrix)

# accuracy_score(test_target, y_pred)