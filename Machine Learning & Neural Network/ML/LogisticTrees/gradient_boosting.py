import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score, roc_auc_score
import matplotlib.pyplot as plt

######################################## Task with mobile phone clients ########################################

data = pd.read_csv('E:\Project\practice-loopa\ML & NN\ML\LogisticTrees\\train.csv')

num_cols = [
    'ClientPeriod',
    'MonthlySpending',
    'TotalSpent'
] # Числовые признаки

cat_cols = [
    'Sex',
    'IsSeniorCitizen',
    'HasPartner',
    'HasChild',
    'HasPhoneService',
    'HasMultiplePhoneNumbers',
    'HasInternetService',
    'HasOnlineSecurityService',
    'HasOnlineBackup',
    'HasDeviceProtection',
    'HasTechSupportAccess',
    'HasOnlineTV',
    'HasMovieSubscription',
    'HasContractPhone',
    'IsBillingPaperless',
    'PaymentMethod'
] # Категориальные признаки

feature_cols = num_cols + cat_cols
target_col = 'Churn'

data.isna().sum() # подсчет НаНов по колонкам, вывод в виде Series
print(data.info())

# Заметим, что в колонке TotalSpent данные - строки. Если посмотреть внимательно, то есть пустые строки, что может в дальнейшем может привести к ошибке
# Тут идет конвертация данных в числовые
data['TotalSpent'] = data['TotalSpent'].str.replace(' ','0')
data['TotalSpent'][1048]
data['TotalSpent'] = pd.to_numeric(data['TotalSpent'])

print("Гистограммы числовых признаков")
for i in num_cols:
  plt.hist(data[i].values, label=i)
  plt.legend()
#   plt.show()
print("Гистограммы категориальных признаков по отдельности")
for j in cat_cols:
  print(j)
  plt.bar(data[j].value_counts().index, data[j].value_counts().values, label=j, width=0.5, align='edge')
  plt.legend()
#   plt.show()
print("Гистограмма всех категориальных признаков")
fig, ax = plt.subplots()
for j in cat_cols:
  #print(j)
  ax.bar(data[j].value_counts().index, data[j].value_counts().values, label=j, width=0.5, align='edge')
  ax.legend()
# plt.show()
plt.bar(data[target_col].value_counts().index, data[target_col].value_counts().values, label=j, width=0.5)

data.drop("HasPhoneService", axis=1)
data.drop("IsSeniorCitizen", axis=1)

from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, RobustScaler, LabelEncoder, OneHotEncoder
from sklearn.pipeline import make_pipeline
import catboost

######################################## Task with mobile phone clients ########################################

# learn by regression

# преобразую данные. В фрейме X категориальные данные преобразованы для обучения LogisticRegression,
# в X_origin данные не изменены (потом будет использован в catboost) 
numeric_data = data[num_cols]
categorial_data = data[cat_cols]
scaler = StandardScaler()
X_scaled = pd.DataFrame(scaler.fit_transform(numeric_data))
dummy_features = pd.get_dummies(categorial_data)
X = pd.concat([X_scaled, dummy_features], axis=1)
X_origin = data.iloc[:, :-1]
y = data['Churn']

# training
X_train_origin, X_test_origin, y_train, y_test = train_test_split(
    X_origin.values, 
    y.values, 
    train_size=0.8, 
    random_state=42
)

boosting_model = catboost.CatBoostClassifier(
    n_estimators=250, 
    cat_features=np.array([data.columns.get_loc(c) for c in cat_cols if c in data]),silent=True, eval_metric='AUC'
)

#boosting_model.fit(X_train_origin, y_train)

'''y_train_predicted = boosting_model.predict_proba(X_train_origin)[:, 1]
y_test_predicted = boosting_model.predict_proba(X_test_origin)[:, 1]
train_auc = roc_auc_score(y_train, y_train_predicted)
test_auc = roc_auc_score(y_test, y_test_predicted)
print(train_auc)
test_auc'''
boosting_model.grid_search(
    {'learning_rate': np.linspace(0, 0.1, 100)}, 
    X_train_origin, 
    y_train, 
    plot=True, 
    refit=True
)

best_model = boosting_model.fit(X_train_origin, y_train)
y_test_predicted = boosting_model.predict_proba(X_test_origin)[:, 1]
test_auc = roc_auc_score(y_test, y_test_predicted)
print(test_auc)