import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score


# data processing

all_data = pd.read_csv('E:\\Project\\practice-loopa\\ML & NN\\ML\\KNN sample\\forest_dataset.csv')

# target variable is last (54)

target = all_data[all_data.columns[-1]].values
feature_matrix = all_data[all_data.columns[:-1]].values

train_feature_matrix, test_feature_matrix, train_target, test_target = train_test_split(feature_matrix, target, test_size=0.2, random_state=42)

# KNN on 10 neighbours

classificator = KNeighborsClassifier(n_neighbors=10)

classificator.fit(train_feature_matrix, train_target)
prediction = classificator.predict(test_feature_matrix)

print(accuracy_score(test_target, prediction))

# Try GridSearchCV

param_grid = {
    'n_neighbors': np.arange(1, 12),
    'weights': ['uniform', 'distance'],
    'metric': ['minkowski', 'manhattan', 'euclidean']
}

cv_search = GridSearchCV(classificator, param_grid, cv=5, scoring='accuracy', n_jobs=-1)
cv_search.fit(feature_matrix, target)

print(cv_search.best_params_)
