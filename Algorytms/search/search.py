def linear_search(array, key):
    """
    Keyword arguments:
    array -- array of elements\n
    key -- index of searching element (int)\n

    Return:
    index -- index of searching element (int)\n
    -1 -- element wasn't found\n

    Difficult -- O(n)

    """

    for i in range(len(array)):
        if array[i] == key:
            return i
    
    return -1

def binary_search(sorted_array, key):
    """ Right-side binary search with the completion of the search, when the element is found early
    Keyword arguments:
    array -- array of elements, \n
    key -- index of searching element (int) \n

    Return:
    index -- index of searching element (int) \n
    -1 -- element wasn't found

    Difficult -- O(log2(n))

    """

    right_border = len(sorted_array)
    left_border = -1

    while right_border > left_border + 1:

        center = (right_border + left_border) // 2

        if sorted_array[center] == key:
            return center

        if sorted_array[center] < key:
            left_border = center
        else:
            right_border = center
            
    return right_border    

print(binary_search([-2.0, 1.0, 3.0, 5.0, 28.0], 0.0))