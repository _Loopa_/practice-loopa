package main

import (
	"errors"
	"fmt"
)

// Linear search for float array
func linear_search(array []float64, key float64) int64 {

	for i := 0; i < len(array); i++ {
		if array[i] == key {
			return int64(i)
		}
	}

	return -1
}

// left-side Binary search for sorted float array with the completion of the search, when the element is found early
func binary_search(sorted_array []float64, key float64) (int, error) {

	var left_border int = -1
	var right_border int = len(sorted_array)

	for right_border > left_border+1 {

		var center int = (left_border + right_border) / 2

		if sorted_array[center] == key {
			return center, nil
		}

		if sorted_array[center] > key {
			right_border = center
		} else {
			left_border = center
		}
	}

	if sorted_array[left_border] == key {
		return left_border, nil
	} else {
		return len(sorted_array), errors.New("logic error: there is no such element in array")
	}

}

func main() {
	test_array := []float64{0.0, 2.0, 3.0, 5.0, 19.0, 28.0}
	search_result, err := binary_search(test_array, 4.0)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(search_result)
}
