def factorization(number):
    '''
    Prime factorization of a number
    
    :param number: factorized number
    :type number: int

    :return: array of prime factors
    :rtype: list
    '''
    i = 2
    factors = []
    while number > 1:
        while ((number % i) == 0):
            print(i)
            number = number / i
            factors.append(i)
        i += 1
    return factors

def NOD(a, b):
    '''
    return greatest common divisor of two int numbers

    :param a: first number
    :type a: int

    :param b: second number
    :type b: int

    :return: NOD of number a & b
    :rtype: int
    '''
    while (a - b) != 0:
        if a > b:
            a -= b
        else:
            b -= a
    return a

def fractionSum(a, b, c, d):
    """
    Print in terminal irreducible fraction resulting from summation a/b + c/d

    :param a: numerator of first fraction
    :type a: int

    :param b: denomerator of first fraction
    :type b: int

    :param c: numerator of second fraction
    :type c: int

    :param d: denomerator of second fraction
    :type d: int

    :return: print numerator and denomenator of result fraction
    """
    m = a * d + b * c
    n = b * d
    nod = NOD(m, n)
    m = m / nod
    n = n / nod
    print("numerator %d denominator %d\n", m, n)

def quick_row(number, row):
    """
    return result of "number" powered by "row"
    
    :param number: number which is powered
    :type number: float

    :param row: power
    :type row: int

    :return: number ^ row
    :rtype: float
    """
    if row == 0: return 1
    else:
        if row % 2 == 0:
            step = quick_row(number, row / 2)
            return  step * step
        else: return quick_row(number, row - 1) * number

