package main

import (
	"fmt"
)

func factorization(n int) map[int]int {
	i := 2
	factors := make(map[int]int)
	for n > 1 {
		for (n % i) == 0 {
			fmt.Println(i)
			fmt.Println(" ")
			n = n / i
			factors[i] += 1
		}
		i += 1
	}
	return factors
}

// function returns minimal common divider of two numbers
func NOD(firstNumber int, secondNumber int) int {
	for (firstNumber - secondNumber) != 0 {
		if firstNumber > secondNumber {
			firstNumber -= secondNumber
		} else {
			secondNumber -= firstNumber
		}
	}
	return firstNumber
}

// function returns undeletable fraction of a/b + c/d
func fractionSum(firstNumerator int, firstDenumerator int, secondNumerator int, secondDenumerator int) {
	var resultNumerator, resultDenumerator, nod int

	resultNumerator = firstNumerator*secondDenumerator + firstDenumerator*secondNumerator
	resultDenumerator = firstDenumerator * secondDenumerator

	nod = NOD(resultNumerator, resultDenumerator)
	resultNumerator = resultNumerator / nod
	resultDenumerator = resultDenumerator / nod

	fmt.Printf("numerator %d denominator %d\n", resultNumerator, resultDenumerator)
}

func quickRow(number float64, row int) float64 {
	if row == 0 {
		return 1
	} else {
		if row%2 == 0 {
			step := quickRow(number, row/2)
			return step * step
		} else {
			return quickRow(number, row-1) * number
		}
	}
}

func DigitalSum(number int) int {
	rest := number % 10
	sum := 0
	for number != 0 {
		sum += rest
		number = number / 10
		rest = number % 10
	}
	return sum
}

// codewars
func DigitalRoot(n int) int {
	DigitalSum := func(number int) int {
		rest := number % 10
		sum := 0
		for number != 0 {
			sum += rest
			number = number / 10
			rest = number % 10
		}
		return sum
	}

	for n/10 != 0 {
		n = DigitalSum(n)
	}
	return n
}

// codewars, return sides of squares in rectangle
func SquaresInRect(lng int, wdth int) []int {
	var output []int

	if (lng == 1) || (wdth == 1) {
		elem_squares_number := 0
		if lng > wdth {
			elem_squares_number = lng
		} else {
			elem_squares_number = wdth
		}
		for i := 0; i < elem_squares_number; i++ {
			output = append(output, 1)
		}
	} else if (lng == wdth) && (lng > 1) {
		output = append(output, lng)
	} else {
		if lng > wdth {
			squares_number := lng / wdth
			for i := 0; i < squares_number; i++ {
				output = append(output, wdth)
			}
			if lng%wdth != 0 {
				output = append(output, SquaresInRect(lng-squares_number*wdth, wdth)...)
			}
		} else if lng < wdth {
			squares_number := wdth / lng
			for i := 0; i < squares_number; i++ {
				output = append(output, lng)
			}
			if wdth%lng != 0 {
				output = append(output, SquaresInRect(lng, wdth-squares_number*lng)...)
			}
		}

	}
	return output
}

func main() {
	fmt.Println("391884 factorization:")
	m := factorization(391884)
	for k, v := range m {
		fmt.Printf("key: %v, value: %v\n", k, v)
	}
	println("NOD two numbers above")
	println(NOD(391884, 1984))
	fractionSum(20, 34, 22, 94)
	println(quickRow(2.5, 8))
	fmt.Println(DigitalRoot(195))
	fmt.Println(SquaresInRect(20, 14))
}
