package main

import (
	"container/list"
	"fmt"
)

// условия задач: https://contest.yandex.ru/contest/17491/problems/

func facrorialZeros(n int) int { // task 1_1
	var zeros int = 0
	for i := 1; i <= n; i++ {
		step := i
		for (step % 5) == 0 {
			step = step / 5
			zeros += 1
		}
	}
	return zeros
}

func primeFactorization(n int) { // task 1_2
	i := 2
	for n > 1 {
		for (n % i) == 0 {
			fmt.Println(i)
			fmt.Println(" ")
			n = n / i
		}
		i += 1
	}
}

func NOD(a int, b int) int {
	for (a - b) != 0 {
		if a > b {
			a -= b
		} else {
			b -= a
		}
	}
	return a
}

func fractionSum(a int, b int, c int, d int) { // task 1_3
	var m, n, nod int
	m = a*d + b*c
	n = b * d
	nod = NOD(m, n)
	m = m / nod
	n = n / nod
	fmt.Printf("numerator %d denominator %d\n", m, n)
}

func maxNod(S int) { // task 1_4
	i := 2
	for (S % i) != 0 {
		i += 1
	}
	fmt.Printf("A = %d B = %d\n", i, S-i)
}

func squares(n int) { // task 1_5
	i := 1
	square := i
	for n*n >= square {
		println(square)
		i += 2
		square += i
	}
}

func returnArray(size int, array []int) []int { // task 1_6
	swap := 0
	for i := 0; i < size/2; i++ {
		swap = array[i]
		array[i] = array[size-i-1]
		array[size-i-1] = swap
	}
	return array
}

func max_pair(size_1 int, array_1 []int, size_2 int, array_2 []int, k int) int { // task 2_3
	pairs := 0
	ptr_1 := 0
	ptr_2 := 0
	for array_1[ptr_1] < array_2[size_2-1-ptr_2] {
		if array_1[ptr_1]+array_2[size_2-1-ptr_2] > k {
			ptr_2++
		} else if array_1[ptr_1]+array_2[size_2-1-ptr_2] < k {
			ptr_1++
		} else {
			ptr_1++
			ptr_2++
			pairs++
		}
	}
	return pairs
}

func polygon_square(points int, coordinates [][]int) float64 { // task 2_2
	S := 0.0
	for i := 0; i < points; i++ {
		if i != points-1 {
			S += float64((coordinates[i+1][1]+coordinates[i][1])*(coordinates[i+1][0]-coordinates[i][0])) / 2
		} else {
			S += float64((coordinates[0][1]+coordinates[i][1])*(coordinates[0][0]-coordinates[i][0])) / 2
		}
	}
	return S
}

func maxSum(size int, A []int, B []int) (ptr_1, ptr_2 int) { // task 2_1
	max_element_left := func(array []int) []int {
		out := make([]int, size)
		max := array[0]
		max_ptr := 0
		for i, value := range array {
			if value > max {
				max = value
				max_ptr = i
				out[i] = i
			} else {
				out[i] = max_ptr
			}
		}
		return out
	}

	max_element_right := func(array []int) []int {
		out := make([]int, size)
		max := array[size-1]
		max_ptr := size - 1
		for i := len(array) - 1; i >= 0; i-- {
			if array[i] >= max {
				max = array[i]
				max_ptr = i
				out[i] = i
			} else {
				out[i] = max_ptr
			}
		}
		return out
	}

	A_ptr := max_element_left(A)
	B_ptr := max_element_right(B)

	maxSum := A[A_ptr[0]] + B[B_ptr[0]]
	ptr_1 = A_ptr[0]
	ptr_2 = B_ptr[0]

	for i := 0; i < size; i++ {
		if A[A_ptr[i]]+B[B_ptr[i]] > maxSum {
			maxSum = A[A_ptr[i]] + B[B_ptr[i]]
			ptr_1 = A_ptr[i]
			ptr_2 = B_ptr[i]
		}
	}

	return ptr_1, ptr_2
}

func counter(N int, k int) interface{} { // task 2_4
	members := list.New()
	for i := 1; i <= N; i++ {
		_ = members.PushBack(i)
	}

	current_element := members.Front()
	remove_elem := current_element
	for members.Len() != 1 {
		for count := 1; count < k; count++ {
			current_element = current_element.Next()
			if current_element == nil {
				current_element = members.Front()
			}
		}
		remove_elem = current_element
		current_element = current_element.Next()
		if current_element == nil {
			current_element = members.Front()
		}
		members.Remove(remove_elem)
	}

	return members.Front().Value
}

// left-side Binary search for sorted float array with the completion of the search, when the element is found early
func left_binary_search(sorted_array []float64, key float64) int {

	var left_border int = -1
	var right_border int = len(sorted_array)

	for right_border > left_border+1 {

		var center int = (left_border + right_border) / 2

		if sorted_array[center] == key {
			return center
		}

		if sorted_array[center] > key {
			right_border = center
		} else {
			left_border = center
		}
	}

	if sorted_array[left_border] == key {
		return left_border
	} else {
		return len(sorted_array)
	}

}

func left_binary_search_int(sorted_array []int, key int) int {

	var left_border int = -1
	var right_border int = len(sorted_array)

	for right_border > left_border+1 {

		var center int = (left_border + right_border) / 2

		if sorted_array[center] == key {
			return center
		}

		if sorted_array[center] > key {
			right_border = center
		} else {
			left_border = center
		}
	}

	if sorted_array[left_border] == key {
		return left_border
	} else {
		return len(sorted_array)
	}

}

func NextElement(size_A int, size_B int, A []int, B []int) []int { // task 3_1
	binary_search_3_1 := func(sorted_array []int, key int) int {

		var left_border int = -1
		var right_border int = len(sorted_array)

		for right_border > left_border+1 {

			var center int = (left_border + right_border) / 2

			if sorted_array[center] == key {
				return center
			}

			if sorted_array[center] > key {
				right_border = center
			} else {
				left_border = center
			}
		}

		return right_border
	}

	output := make([]int, size_B)
	for i := 0; i < size_B; i++ {
		output[i] = binary_search_3_1(A, B[i])
	}
	return output
}

func main() {
	// task 1_1
	zeros := facrorialZeros(39)
	fmt.Println("zeros in factorial:")
	println(zeros)
	// task 1_2
	fmt.Println("391884 factorization:")
	primeFactorization(391884)

	fmt.Println("1984 factorization:")
	primeFactorization(1984)
	// NOD validation
	println("NOD two numbers above")
	println(NOD(391884, 1984))
	// task 1_3
	fractionSum(20, 34, 22, 94)
	// task 1_4
	maxNod(8)
	// task 1_5
	squares(6)
	// task 1_6
	old_arr := []int{2, 3, -1, 6, 29}
	arr := returnArray(5, old_arr)
	for i, v := range arr {
		fmt.Printf("a[%d] = %d\t", i, v)
	}
	fmt.Printf("\n")
	// task 2_3
	arr_1 := []int{-5, 0, 1, 3, 18}
	arr_2 := []int{-21, -20, 4, 6, 7, 12}
	fmt.Printf("pairs %d\n", max_pair(5, arr_1, 6, arr_2, 7))
	// task 2_2
	points := 4
	coords := [][]int{
		{2, 1},
		{1, 1},
		{1, 2},
		{2, 4},
	}

	fmt.Println(polygon_square(points, coords))

	// task 2_1
	a, b := maxSum(5, []int{4, 0, 4, -8, 6}, []int{1, 1, -10, 3, 1})
	fmt.Println(a, b)

	// task 2_4
	fmt.Println(counter(15, 11))

	// task 3_1
	fmt.Println(NextElement(2, 1, []int{1, 2}, []int{2}))

}
