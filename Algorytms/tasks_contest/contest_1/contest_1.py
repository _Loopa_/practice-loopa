from lib2to3.pytree import convert


def facrorialZeros(n): # task 1_1
	zeros = 0
	for i in range(n):
		step = i
		while (step % 5) == 0:
			step = step / 5
			zeros += 1
	return zeros

def factorization(number): # task 1_2
    '''
    Prime factorization of a number
    
    :param number: factorized number
    :type number: int

    :return: array of prime factors
    :rtype: list
    '''
    i = 2
    factors = []
    while number > 1:
        while ((number % i) == 0):
            print(i)
            number = number / i
            factors.append(i)
        i += 1
    return factors

def NOD(a, b):
    '''
    return greatest common divisor of two int numbers

    :param a: first number
    :type a: int

    :param b: second number
    :type b: int

    :return: NOD of number a & b
    :rtype: int
    '''
    while (a - b) != 0:
        if a > b:
            a -= b
        else:
            b -= a
    return a

def convert(rome_number):
    """
    function to convert rome numbers into arabic numbers
    """
    summ = 0

    rome_figures = {
        'I': 1,
        'V': 5,
        'X': 10,
        'L': 50,
        'C': 100,
        'D': 500,
        'M': 1000 
    }

    for i in range(len(rome_number) - 1):
        a = rome_number[i]
        if rome_number[i] in rome_figures:
            if rome_figures[rome_number[i]] < rome_figures[rome_number[i+1]]:
                summ -= rome_figures[rome_number[i]]
            else:
                summ += rome_figures[rome_number[i]]

        else:
            summ = 0
            return summ

    if rome_number[len(rome_number) - 1] in rome_figures.keys():
        summ += rome_figures[rome_number[len(rome_number) - 1]]
    else:
        summ = 0

    return summ

def fractionSum(a, b, c, d): # task 1_3
    """
    Print in terminal irreducible fraction resulting from summation a/b + c/d

    :param a: numerator of first fraction
    :type a: int

    :param b: denomerator of first fraction
    :type b: int

    :param c: numerator of second fraction
    :type c: int

    :param d: denomerator of second fraction
    :type d: int

    :return: print numerator and denomenator of result fraction
    """
    m = a * d + b * c
    n = b * d
    nod = NOD(m, n)
    m = m / nod
    n = n / nod
    print("numerator %d denominator %d\n", m, n)

def maxNod(S): # task 1_4
	i = 2
	while (S % i) != 0:
		i += 1
	print("A = %d B = %d\n", i, S-i)

def squares(n): # task 1_5
	i = 1
	square = i
	while n**2 >= square:
		print(square)
		i += 2
		square += i

def returnArray(size, array): # task 1_6
    new_array = array.copy()
    for i in range(size):
        new_array[-i-1] = array[i]
    return new_array

def squared(x):
    return x ** 2

def calc(func, a, b=True, c=False):
    if b:
        return func(a)
    else:
        if c:
            return a
        else:
            return 0

import multiprocessing as mp
import time

if __name__ == '__main__':

    start_1 = time.time()
    pool = mp.Pool(3)

    a1, a2, a3 = 1.0, 2.0, 3.0
    c=True
    b=False

    var = (
        (squared, a1, b, c),
        (squared, a2, b, c),
        (squared, a3, b, c)
    )

    result = pool.map(calc, var)
    print(result, result.type())
    print("time: ", time.time() - start_1)

    start_2 = time()

    print(calc(squared, a1, b, c))
    print(calc(squared, a2, b, c))
    print(calc(squared, a3, b, c))

    print("time: ", time.time() - start_2)