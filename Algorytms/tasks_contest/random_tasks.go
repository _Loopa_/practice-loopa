package main

import (
	"fmt"
	"strconv"
	"strings"
)

func MoveZeros(arr []int) []int {
	swap := func(a, b *int) {
		tmp := *a
		*a = *b
		*b = tmp
	}
	swap_zero := func(i int) int {
		j := i
		for arr[j] == 0 && j != len(arr)-1 {
			j++
		}
		if arr[j] == 0 {
			return len(arr) - 1
		}
		swap(&arr[i], &arr[j])
		i++
		return i
	}

	i := 0
	for arr[i] != 0 && i != len(arr)-1 {
		i++
	}

	for i != (len(arr) - 1) {
		i = swap_zero(i)
	}

	return arr
}

func SpinWords(str string) string {
	reverseWord := func(str string) string {
		reverse := ""
		for i := len(str) - 1; i >= 0; i-- {
			reverse += string(str[i])
		}
		return reverse
	}

	words := strings.Fields(str)

	for key, word := range words {
		if len(word) >= 5 {
			words[key] = reverseWord(word)
		}
	}

	return strings.Join(words, " ")
}
func CreatePhoneNumber(numbers [10]uint) string {
	var str_numbers [10]string
	for key, value := range numbers {
		str_numbers[key] = strconv.FormatUint(uint64(value), 10)
	}
	phone_number := "(" + strings.Join(str_numbers[:3], "") + ") " + strings.Join(str_numbers[3:6], "") + "-" + strings.Join(str_numbers[6:], "")
	return phone_number
}

func ValidBraces(str string) bool {
	brackets := strings.Split(str, "")
	bracket_stack := brackets[:0]
	for _, letter := range brackets {
		if letter == "(" || letter == "[" || letter == "{" {
			bracket_stack = append(bracket_stack, letter)
		} else if (len(bracket_stack) > 0) && ((letter == ")" && bracket_stack[len(bracket_stack)-1] == "(") || (letter == "]" && bracket_stack[len(bracket_stack)-1] == "[") || (letter == "}" && bracket_stack[len(bracket_stack)-1] == "{")) {
			bracket_stack = bracket_stack[:len(bracket_stack)-1]
		} else {
			return false
		}
	}
	if len(bracket_stack) == 0 {
		return true
	} else {
		return false
	}
}

func splitNumber(number int) []int {
	digits := []int{}
	number_copy := number
	for number_copy != 0 {
		digits = append([]int{number_copy % 10}, digits...)
		number_copy /= 10
	}
	return digits
}

func Thirt(n int) int {
	splitNumber := func(number int) []int {
		digits := []int{}
		number_copy := number
		for number_copy != 0 {
			digits = append(digits, number_copy%10)
			number_copy /= 10
		}
		return digits
	}

	// for int64 max value
	modesOfTen := [6]int{1, 10, 9, 12, 3, 4}
	stationary := 0

	digits := splitNumber(n)

	for key, value := range digits {
		stationary += value * modesOfTen[key%6]
	}
	if stationary == n {
		return stationary
	} else {
		return Thirt(stationary)
	}
}

func Beeramid(bonus int, price float64) int {
	levelBeeramid := func(cans int) int {
		level := 1
		for cans >= 0 {
			cans -= level * level
			level += 1
		}
		return level - 2
	}

	var cans int
	if bonus >= 0 {
		cans = int(float64(bonus) / price)
	} else {
		cans = 0
	}
	return levelBeeramid(cans)
}

func main() {
	test_array_3 := [10]uint{1, 0, 0, 2, 3, 4, 0, 0, 5, 6}

	fmt.Println(CreatePhoneNumber(test_array_3))
	fmt.Println(ValidBraces("[(){}{{}(])}]"))
	fmt.Println(SpinWords("Hey fellow warriors"))
	fmt.Println(splitNumber(12345))
	fmt.Println(Beeramid(-10, 2.0))
}
