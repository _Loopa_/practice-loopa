package main

import (
	"math/rand"
	"time"
)

func generateString() string {
	var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789")
	rand.Intn(len(letters))
	rand.Seed(time.Now().UnixNano())

	b := make([]rune, 10)

	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}

func main() {

}
