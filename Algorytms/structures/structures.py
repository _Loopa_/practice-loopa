from queue import Queue


class ForwardList:
    class Node:
        def __init__(self, data):
            self.__data = data
            self.__next = None

        @property
        def data(self):
            return self.__data

        @data.setter
        def data(self, object):
            self.__data = object

        @property
        def next(self):
            return self.__next

        @next.setter
        def next(self, pointer):
            self.__next = pointer        

    def __init__(self):
        self.__head = None
        self.__size = 0
    
    @property
    def size(self):
        return self.__size

    @property
    def head(self):
        return self.__head
    # def generator(self):
    #     iter = self.__head
    #     while iter is not None:
            
    #         yield self.__head

    def push_back(self, element):

        node = self.Node(element)

        if self.__size == 0:
            self.__head = node
            self.__size += 1

        else:
            current = self.__head

            while current.next:
                current = current.next
            
            current.next = node
            self.__size += 1

    def push_front(self, element):
        
        node = self.Node(element)

        if self.size == 0:
            self.__head = node

        else:
            old_head = self.__head
            self.__head = node
            node.next = old_head
        
        self.__size += 1

    def pop_front(self):
        if self.__size == 1:
            
            del self.__head
            self.__head = None
            self._size = 0

        elif self.__size >= 1:
            new_head = self.__head.next
            del self.__head
            self.__head = new_head

    def print(self):

        current = self.__head

        while current.next:
            print(current.data)
            current = current.next
        
        print(current.data)

        del current
class ManagerTree():  
    '''
    Task from Yandex.Profi AI 2022 final, task 3
    realization with id of point
    '''
    class Point():
        def __init__(self, id):
            self.id = id
            self.role = None
            self.parent = None
            self.children = {} # list with child id and his math weight
        
        def add_child(self, id_child, weight):
            self.children[id_child] = weight

        def add_parent(self, parent):
            self.parent = parent

        # def get_id(self):
        #     return self.id

        # def get_parent(self):
        #     return self.parent

        # def get_childs(self):
        #     return self.children

        # def get_role(self):
        #     return self.role

        def update_role(self, new_role):
            self.role = new_role

        def update_weight(self, id, weight):
            self.children[id] = weight
          
    def __init__(self):
        self.Points = [self.Point(1)]

    def get_point_by_id(self, id):
        for point in self.Points:
            if point.id == id:
                return point

    def add_Point(self, id, parent, role):
        new_point = self.Point(id)
        new_point.update_role(role)
        new_point.add_parent(parent)
        parent = self.get_point_by_id(parent)
        parent.add_child(id, 0)
        self.Points.append(new_point)

    def get_head(self):
        return self.Points[0]

    def update_tree(self):
        for Point in self.Points:
                if Point.role > 0:
                    Point.children = None
                    self.get_point_by_id(Point.parent).update_weight(Point.id, Point.role)
                
                if Point.role == 0:
                    expectation = 0
                    
                    for child_id in Point.children.keys():
                        expectation += Point.children[child_id]

                    expectation = len(Point.children)
                    
                    if Point.id == 1:
                        Point.parent = expectation
                    else:
                        self.get_point_by_id(Point.parent).update_weight(Point.id, expectation)
                
                if Point.role == -1:
                    expectation = max(Point.children, key=Point.children.get)
                    self.get_point_by_id(Point.parent).update_weight(Point.id, expectation)

class BinaryTree():
    class Vertex():
        def __init__(self, weight=None, left_child=None, right_child=None):
            self.__weight = weight
            self.__left_child = left_child
            self.__right_child = right_child

        @property
        def weight(self):
            return self.__weight

        @property
        def left_child(self):
            return self.__left_child

        @property
        def right_child(self):
            return self.__right_child

        @weight.setter
        def weight(self, new_weight):
            self.__weight = new_weight

        @left_child.setter
        def left_child(self, Vertex):
            self.__left_child = Vertex

        @right_child.setter
        def right_child(self, Vertex):
            self.__right_child = Vertex

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls) 

    @property
    def root(self):
        return self.__root

    @property
    def depth(self):
        return self.__depth

    def __init__(self, array:list = None):
        
        self.__root = self.Vertex()
        self.__depth = 1

        if array is not None:
            element = iter(array)
            def fill_by_list(self, current_vertex, iterator):
                try:
                    current_vertex.weight = next(iterator)
                    current_vertex.left_child = self.Vertex()                    
                    current_vertex.right_child = self.Vertex()
                except StopIteration:
                    del current_vertex
                    print('tree is built')
                
                try:
                    fill_by_list(self, current_vertex.left_child, iterator)
                    self.__depth += 1
                except StopIteration:
                    del current_vertex.left_child
                    print('tree is built')

                try:
                    fill_by_list(self, current_vertex.right_child, iterator)
                except StopIteration:
                    del current_vertex.left_child
                    print('tree is built')

            fill_by_list(self, self.__root, element)

        # @classmethod
        # def create_binary_heap(cls, array):
        #     return 0

    def search(self, place):
        """
        BFS search
        """
        q = Queue()
        i = 0
        
        point = self.__root
        q.put(point.get_left_child())
        q.put(point.get_right_child())
        
        while i < place:
            point = q.get()
            q.put(point.get_left_child())
            q.put(point.get_right_child())
        
        return point
        

    def insert(self, place, weight=0, left=True):
        parent=self.search(place)
        vertex = self.Vertex(weight)
        if left is True and parent.get_left_child() == None:
            parent.set_left_child(vertex)
        elif left is True and parent.get_left_child() != None:
            parent.set_left_child(vertex)

class Tree():
    """
    Tree without elements in point, with pointers, elements have saved in lists
    """
    class Point():
        def __init__(self, parent=None, children=[]):
            self.parent = parent
            self.children = children # list with children

        def add_parent(self, parent):
            self.parent = parent  
        
        def add_children(self, child):
            self.children.append(child)

        def get_parent(self):
            return self.parent

        def get_children(self):
            return self.children

    _head = None
    _depth = 0

    def __init__(self):
        self._head = self.Point()
        self._depth = 1
   
class ArrayTree():
    def __init__(self):
        self.__tree = [{"data": 0, "children": []}]
    
    def search(self, id):
        return self.__tree[id]

    def get_elem(self, id):
        return self.__tree[id]["data"]

    def set_elem(self, id, new_data):
        self.__tree[id]["data"] = new_data

    def insert(self, parent, data):
        self.__tree.append({"data": data, "children": []})
        self.search(parent)["children"].append(len(self.__tree) - 1)
        
    def print_elem(self):
        out = []
        for i in range(len(self.__tree)):
            out.append(self.__tree[i]["data"])
        print(out)

# Test_list = ForwardList()
# Test_list.push_front(112)
# Test_list.push_front([1, 2])
# Test_list.push_front('this is a list')
# Test_list.push_back((1, 2, 10))
# Test_list.push_front(200)
# Test_list.pop_front()
# Test_list.print()

# print(Test_list.size)
# print(Test_list.head.data)

T = BinaryTree([1, 3, 2, 10, 14, -20])