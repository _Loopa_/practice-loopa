def selection_sort(array):
    """ Sort-algorythm of array by selection, array elements are keys
    Keyword arguments:
    array -- array of elements, \n

    Return:
    sorted_array

    Difficult -- O(n ** 2)

    """

    for i in range(len(array)-1):

        min = i

        for j in range(i+1, len(array)):

            if array[j] < array[min]:
                min = j
        
        swap_elem = array[i]
        array[i] = array[min]
        array[min] = swap_elem
    
    return array

print(selection_sort([2.0, -1.0, -3.0, -5.0, -28.0]))