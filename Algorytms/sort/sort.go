package main

import (
	"fmt"
)

//Sort-algorythm of array by selection, array elements are keys
func selection_sort(array []float64) []float64 {

	for i := 0; i < len(array)-1; i++ {

		var min int = i

		for j := i + 1; j < len(array); j++ {

			if array[j] < array[min] {
				min = j
			}

		}

		var swap_elem float64 = array[min]
		array[min] = array[i]
		array[i] = swap_elem

	}

	return array

}

func quickSort(array []int) []int {
	// Hoare's scheme
	swap := func(a, b *int) {
		tmp := *a
		*a = *b
		*b = tmp
	}

	partition := func(array []int) int {
		pivot := len(array) / 2
		i := 0
		j := len(array) - 1
		for i <= j {
			for array[i] <= array[pivot] && i < pivot {
				i++
			}
			for array[j] > array[pivot] && j > pivot {
				j--
			}
			if i >= j {
				break
			}
			if j == pivot {
				pivot = i
			} else if i == pivot {
				pivot = j
			}
			swap(&array[i], &array[j])
		}
		return j
	}

	if len(array) < 2 {
		return array
	} else {
		pivot := partition(array)
		var output []int = quickSort(array[:pivot])
		output = append(output, array[pivot])
		output = append(output, quickSort(array[pivot+1:])...)
		return output
	}
}

func main() {
	test_array := []float64{2.0, -1.0, -3.0, -5.0, -28.0}
	test_array_2 := []int{1, 2, 3, 4, 2, 1}

	fmt.Println(quickSort(test_array_2))
	fmt.Println(selection_sort(test_array))
}
