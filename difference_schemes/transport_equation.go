package main

import (
	"fmt"
	"math"
)

func leftCornerScheme(u []float64, kurrant float64, time_steps int, x_points uint64) []float64 {
	u_new := make([]float64, x_points)
	for i := 0; i < time_steps; i++ {
		for j := 0; j < int(x_points)-1; j++ {
			u_new[j+1] = u[j+1] - kurrant*(u[j+1]-u[j])
		}
		copy(u, u_new)
	}
	return u
}

func main() {
	k := 2.0
	kurrant := 1.0
	h_step := 1.0
	t_step := kurrant * h_step
	t_points := 1000
	x_points := 3000
	u_grid := make([]float64, x_points)
	for i := 0; i < x_points; i++ {
		u_grid[i] = 0.0
	}
	t_finish := 38

	t_grid := make([]float64, t_points)
	for i := 0; i < t_points; i++ {
		t_grid[i] = float64(i) * t_step
	}

	x_grid := make([]float64, x_points)
	for i := 0; i < x_points; i++ {
		x_grid[i] = float64(i) * h_step
	}

	for i := 1480; i < 1520; i++ {
		u_grid[i] = math.Pow(math.Sin((x_grid[i]-1480.0)*math.Pi/40), k)
	}

	for i := 1470; i < 1530; i++ {
		fmt.Printf("u[%v] = %f\n", i, u_grid[i])
	}

	fmt.Printf("output array\n")
	output := leftCornerScheme(u_grid, kurrant, t_finish, uint64(x_points))
	for i := 1500; i < 1580; i++ {
		fmt.Printf("out[%v] = %f\n", i, output[i])
	}
}
