import numpy as np
import matplotlib.pyplot as plt


def corner_scheme(u, t_steps, x_points):
    u_old = u
    u_new = np.zeros(x_points)
    for i in range(t_steps):
        for j in range(1480, 1520):
            u_new[j] = -kurrant * (u_old[j] - u_old[j - 1]) + u_old[j]
        u_old = u_new

    return u_new


def hoin_scheme(u, t_steps, x_points):
    u_old = u
    u_new = np.zeros(x_points)
    for i in range(t_steps):
        for j in range(1480, 1520):
            u_new[j] = -kurrant * (u_old[j - 2] - 4 * u_old[j - 1] + 3 * u_old[j]) / 2 + u_old[j]
        u_old = u_new

    return u_new


def three_point_scheme(u, t_steps, x_points):
    u_old = u
    u_new = np.zeros(x_points)
    for i in range(t_steps):
        for j in range(1480, 1520):
            u_new[j] = -kurrant * (u_old[j + 3] - u_old[j - 3] + 9 * (u_old[j - 2] - u_old[j + 2]) + 45 * (
                        u_old[j + 1] - u_old[j - 1])) / 60 + u_old[j]
        u_old = u_new

    return u_new


k = 2.0
kurrant = 1.0
h_step = 1.0
t_step = kurrant * h_step
t_points = 1000
x_points = 3000
u_grid = np.zeros(x_points)

t_finish = 38

t_grid = np.arange(0.0, t_points * t_step, t_step)
x_grid = np.arange(0.0, x_points * h_step, h_step)
  
u_0 = np.sin((x_grid[1480:1520] -1480.0) * np.pi / 40) ** k
u_grid[1480:1520] = u_0

def corner_scheme(u, t_steps):
  u_new = u
  for i in range(t_steps):
    u_new[1:] = u[1:] - kurrant*(u[1:] - u[:-1])
    u_new[0] = 0.0
    u = u_new

  return u_new

plt.subplot(1, 2, 1)
plt.xlim(1400, 1700)
plt.plot(x_grid, u_grid)
plt.grid()
plt.title('u[t=0]')
plt.subplot(1, 2, 2)

plt.xlim(1400, 1700)
plt.plot(x_grid, corner_scheme(u_grid, t_finish))
plt.grid()
plt.title('u[t=t_finish]')

plt.show()

def upwind_scheme(u, t_steps, x_points):
  u_new = u
  for i in range(t_steps):
    u[2:] = u[2:] - kurrant * (u[2:] - 4 * u[1:-1] + 3 * u[:-2]) / 2
    u_new[1] = 0.0
    u_new[0] = 0.0
    u = u_new

  return u_new

u_grid = np.zeros(x_points)
u_0 = np.sin((x_grid[1480:1520] -1480.0) * np.pi / 40) ** k
u_grid[1480:1520] = u_0

plt.subplot(1, 2, 1)
plt.xlim(1400, 1700)
plt.plot(x_grid, u_grid)
plt.grid()
plt.title('u[t=0]')
plt.subplot(1, 2, 2)

plt.xlim(1400, 1700)
plt.plot(x_grid, corner_scheme(u_grid, t_finish))
plt.grid()
plt.title('u[t=t_finish]')

plt.show()