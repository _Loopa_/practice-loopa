class Point():
        def __init__(self, id):
            self.id = id
            self.role = None
            self.parent = None
            self.children = {} # list with child id and his math weight
        
        def add_child(self, id_child, weight):
            self.children[id_child] = weight

        def add_parent(self, parent):
            self.parent = parent

        # def get_id(self):
        #     return self.id

        # def get_parent(self):
        #     return self.parent

        # def get_childs(self):
        #     return self.children

        # def get_role(self):
        #     return self.role

        def update_role(self, new_role):
            self.role = new_role

        def update_weight(self, id, weight):
            self.children[id] = weight

class Tree():        
    def __init__(self):
        self.Points = [Point(1)]

    def get_point_by_id(self, id):
        for point in self.Points:
            if point.id == id:
                return point

    def add_Point(self, id, parent, role):
        new_point = Point(id)
        new_point.update_role(role)
        new_point.add_parent(parent)
        parent = self.get_point_by_id(parent)
        parent.add_child(id, 0)
        self.Points.append(new_point)

    def get_head(self):
        return self.Points[0]

    def update_tree(self):
        for Point in self.Points:
                if Point.role > 0:
                    Point.children = None
                    self.get_point_by_id(Point.parent).update_weight(Point.id, Point.role)
                
                if Point.role == 0:
                    expectation = 0
                    
                    for child_id in Point.children.keys():
                        expectation += Point.children[child_id]

                    expectation = len(Point.children)
                    
                    if Point.id == 1:
                        Point.parent = expectation
                    else:
                        self.get_point_by_id(Point.parent).update_weight(Point.id, expectation)
                
                if Point.role == -1:
                    expectation = max(Point.children, key=Point.children.get)
                    self.get_point_by_id(Point.parent).update_weight(Point.id, expectation)


N = 5
links = [1, 1, 3, 3]
roles = [0, 7, -1, 3, 4]

tree = Tree()
tree.get_head().update_role(roles[0])

for i in range(1, N):
    tree.add_Point(id=i+1, parent=links[i-1], role=roles[i])

tree.update_tree()
tree.update_tree()

print(tree.get_head().parent)

    

